package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import fr.dawan.springcore.beans.ArticleRepository;
import fr.dawan.springcore.beans.ArticleService;
import fr.dawan.springcore.beans.DataSource;

@Configuration  // => classe de configuration
@ComponentScan("fr.dawan.springcore")   // => scan des classes du package pour trouver les composants
                                        // @Component, @Repository, @Controller,@Service
public class AppConf {
    
    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean et le nom de la méthode
    @Bean
    public DataSource datasource1() { //On déclare un bean qui a pour nom datasource1 et qui est de type DataSource
        return new DataSource();
    }
    
    @Bean
    public DataSource datasource2() {
        return new DataSource();
    }
    
    // L'attribut name de @Bean permet de définir le nom du bean (un ou plusieurs)
    // dans ce cas, le nom de la méthode n'est plus prix en compte
    @Bean(name="repository2")
    // @Primary => s'il y a plusieurs beans du même type, avec un @Autowired, c'est le bean annoté avec @Primary qui sera sélectionner
    public ArticleRepository getRepository() {
        return new ArticleRepository();
    }
    
    
    @Bean(/*initMethod = "initialisation",destroyMethod = "destruction"*/)
    public ArticleService service2(ArticleRepository repository1) {
        return new ArticleService(repository1); 
    }
    
    @Bean
    public ArticleService service3() {
        return new ArticleService(getRepository()); 
    }

}
