package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.ArticleRepository;
import fr.dawan.springcore.beans.ArticleService;
import fr.dawan.springcore.beans.DataSource;


public class App 
{
    public static void main( String[] args )
    {
        // Création du conteneur d'ioc
        ApplicationContext ctx= new AnnotationConfigApplicationContext(AppConf.class);
        System.out.println("---------------------------------------------------");
        
        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DataSource ds1=ctx.getBean("datasource1",DataSource.class);
        System.out.println(ds1);
        
        DataSource ds2=ctx.getBean("datasource2",DataSource.class );
        System.out.println(ds2);
        
        ArticleRepository rep1=ctx.getBean("repository1",ArticleRepository.class );
        System.out.println(rep1);
        
        ArticleRepository rep2=ctx.getBean("repository2",ArticleRepository.class );
        System.out.println(rep2);
        
        ArticleService serv1=ctx.getBean("service1",ArticleService.class);
        System.out.println(serv1);
        
        ArticleService serv2=ctx.getBean("service2",ArticleService.class);
        System.out.println(serv2);
        
        ArticleRepository rep11=ctx.getBean("repository1",ArticleRepository.class );
        System.out.println(rep11);  
        ArticleRepository rep12=ctx.getBean("repository1",ArticleRepository.class );
        System.out.println(rep12);
        
        // Fermeture du context entraine la destruction de tous les beans 
        ((AbstractApplicationContext)ctx).close();
    }
}
