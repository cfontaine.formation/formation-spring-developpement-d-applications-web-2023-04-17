package fr.dawan.springcore.beans;

import java.io.Serializable;

public class DataSource implements Serializable {

    private static final long serialVersionUID = 1L;

    private String url;

    public DataSource() {

    }

    public DataSource(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "DataSource [url=" + url + ", toString()=" + super.toString() + "]";
    }

}
