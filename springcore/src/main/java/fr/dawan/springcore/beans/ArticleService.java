package fr.dawan.springcore.beans;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Resource;

@Service("service1")
@Lazy
public class ArticleService {
    
    
//    @Autowired
//    @Qualifier("repository2")
    @Resource(name="repository2")
    private ArticleRepository repository;

    public ArticleService() {
        System.out.println("Constructeur par défaut Article Service");
    }

    //@Autowired
    public ArticleService(/*@Qualifier("repository1")*/ArticleRepository repository) {
        System.out.println("Constructeur 1 param ArticleService");
        this.repository = repository;
    }

    public ArticleRepository getRepository() {
        return repository;
    }

   // @Autowired
    public void setRepository(/*@Qualifier("repository1")*/ArticleRepository repository) {
        System.out.println("setter ArticleService");
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "ArticleService [repository=" + repository + ", toString()=" + super.toString() + "]";
    }
    
    @PostConstruct
    public void initialisation() {
        System.out.println("initialisation");
    }
    
    @PreDestroy
    public void destruction() {
        System.out.println("destruction");
    }
}
