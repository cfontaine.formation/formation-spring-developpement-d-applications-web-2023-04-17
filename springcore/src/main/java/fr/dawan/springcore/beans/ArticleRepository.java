package fr.dawan.springcore.beans;

import org.springframework.stereotype.Repository;

@Repository("repository1")  
//@Scope("prototype")
// @Primary
public class ArticleRepository {
    
    private String nom;

    public ArticleRepository() {
        System.out.println("Constructeur par défaut Article repository");
    }

    public ArticleRepository(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "ArticleRepository [nom=" + nom + ", toString()=" + super.toString() + "]";
    }
    
    

}
