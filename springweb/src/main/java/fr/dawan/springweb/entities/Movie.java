package fr.dawan.springweb.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false, name = "title_vf")
    private String titleVF;

    @Column(name = "title_vo")
    private String titleVO;

    @Column(nullable = false)
    private int duration;

    @Column(nullable = false, name = "release_date")
    private LocalDate releaseDate;

    @Lob
    private byte[] moviePoster;

    @Column(nullable = false, length = 2000)
    private String summary;

    @Column(name = "ident_video", length = 20)
    private String identVideo;

    @ManyToOne
    private Nation nationality;

    @ManyToOne
    private Genre genre;

    @ManyToMany(mappedBy = "moviesDirected")
    private Set<Participant> directors = new HashSet<>();

    @ManyToMany(mappedBy = "moviesPerformed")
    private Set<Participant> actors = new HashSet<>();

    public Movie() {
    }

    public Movie(String titleVF, LocalDate releaseDate) {
        this.titleVF = titleVF;
        this.releaseDate = releaseDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getTitleVF() {
        return titleVF;
    }

    public void setTitleVF(String titleVF) {
        this.titleVF = titleVF;
    }

    public String getTitleVO() {
        return titleVO;
    }

    public void setTitleVO(String titleVO) {
        this.titleVO = titleVO;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public int getReleaseYear() {
        return releaseDate.getYear();
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public byte[] getMoviePoster() {
        return moviePoster;
    }

    public String getMoviePosterBase64() {
        return Base64.getEncoder().encodeToString(moviePoster);
    }

    public void setMoviePoster(byte[] moviePoster) {
        this.moviePoster = moviePoster;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Nation getNationality() {
        return nationality;
    }

    public void setNationality(Nation nationality) {
        this.nationality = nationality;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Participant> getActors() {
        return actors;
    }

    public void setActors(Set<Participant> actors) {
        this.actors = actors;
    }

    public Set<Participant> getDirectors() {
        return directors;
    }

    public void setDirectors(Set<Participant> directors) {
        this.directors = directors;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getIdentVideo() {
        return identVideo;
    }

    public void setIdentVideo(String identVideo) {
        this.identVideo = identVideo;
    }

    @Override
    public String toString() {
        return "Movie [id=" + id + ", version=" + version + ", titleVF=" + titleVF + ", titleVO=" + titleVO
                + ", duration=" + duration + ", releaseDate=" + releaseDate + ", moviePoster="
                + Arrays.toString(moviePoster) + ", summary=" + summary + ", identVideo=" + identVideo + "]";
    }

}
