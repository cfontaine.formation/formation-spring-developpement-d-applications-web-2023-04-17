package fr.dawan.springweb.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "participants")
public class Participant implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Version
	private int version;

	@Column(nullable = false,length = 60)
	private String firstName;

	@Column( length = 60)
	private String lastName;

	@Column(nullable = false, name = "birth_date")
	private LocalDate birthDate;

	@Column(name = "death_date")
	private LocalDate deathDate;

	@ManyToOne
	private Nation nationality;

	@ManyToMany
	private Set<Movie> moviesDirected = new HashSet<>();

	@ManyToMany
	private Set<Movie> moviesPerformed = new HashSet<>();

	public Participant() {
	}

	public Participant(String firstName, String lastName, LocalDate birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDate getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(LocalDate deathDate) {
		this.deathDate = deathDate;
	}

	public Nation getNationality() {
		return nationality;
	}

	public void setNationality(Nation nationality) {
		this.nationality = nationality;
	}

	public Set<Movie> getMoviesDirected() {
		return moviesDirected;
	}

	public void setMoviesDirected(Set<Movie> moviesDirected) {
		this.moviesDirected = moviesDirected;
	}

	public Set<Movie> getMoviesPerformed() {
		return moviesPerformed;
	}

	public void setMoviesPerformed(Set<Movie> moviesPerformed) {
		this.moviesPerformed = moviesPerformed;
	}
}
