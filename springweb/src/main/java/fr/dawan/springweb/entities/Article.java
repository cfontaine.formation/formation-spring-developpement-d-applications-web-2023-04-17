package fr.dawan.springweb.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.dawan.springweb.enums.Conditionement;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private double prix;

    @Column(nullable = false, name = "date_production")
    private LocalDate dateProduction;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Conditionement conditionement;

    @Lob
    @Column(length = 65000)
    private byte[] photo;

    @JsonIgnore
    @ManyToOne
    private Marque marque;

    @JsonIgnore
    @ManyToMany(mappedBy = "articles")
    private List<Fournisseur> fournisseurs = new ArrayList<>();

    public Article() {
    }

    public Article(String description, double prix, LocalDate dateProduction, Conditionement conditionement) {
        this.description = description;
        this.prix = prix;
        this.dateProduction = dateProduction;
        this.conditionement = conditionement;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public Conditionement getConditionement() {
        return conditionement;
    }

    public void setConditionement(Conditionement conditionement) {
        this.conditionement = conditionement;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", version=" + version + ", description=" + description + ", prix=" + prix
                + ", dateProduction=" + dateProduction + ", conditionement=" + conditionement + "]";
    }

    
}
