package fr.dawan.springweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.dawan.springweb.entities.Article;
import fr.dawan.springweb.repositories.ArticleRepository;
import fr.dawan.springweb.repositories.MovieRepository;

//@Component
public class RepositoryRunner implements ApplicationRunner {

    @Autowired
    private ArticleRepository repository;
    
    @Autowired
    private MovieRepository rep;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
       System.out.println("Repository runner");
       List<Article> lst=repository.findAll();
       for(Article a: lst) {
           System.out.println(a);
       }
       
       lst=repository.findByPrixLessThan(100.0);
       for(Article a: lst) {
           System.out.println(a);
       }
       
       rep.getMovieByDirector("Scott").forEach(System.out::println);
       
       rep.getMovieByDirectorNation("Italie").forEach(System.out::println);
       
       Page<Article> p=repository.findByPrixLessThan(1000.0, PageRequest.of(0, 3
               ,Sort.by("prix").descending()));
       System.out.println(p.getTotalPages());
       System.out.println(p.getTotalElements());
       for(Article a: p.getContent()) {
           System.out.println(a);
       }
       
       repository.findTop2ByPrixGreaterThanOrderByPrixDesc(10.0).forEach(System.out::println);
    }

}
