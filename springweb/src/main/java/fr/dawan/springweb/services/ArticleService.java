package fr.dawan.springweb.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springweb.entities.Article;

public interface ArticleService {
    
    List<Article> getAllArticle();
    
    List<Article> getAllArticle(Pageable pageable);
    
    void deleteArticle(long id);
    
    Article save(Article article);

}
