package fr.dawan.springweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springweb.entities.Article;
import fr.dawan.springweb.repositories.ArticleRepository;
import fr.dawan.springweb.services.ArticleService;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
    
    @Autowired
    private ArticleRepository repository;

    @Override
    public List<Article> getAllArticle() {
        return repository.findAll();
    }
    
    @Override
    public List<Article> getAllArticle(Pageable pageable) {
            return repository.findAll(pageable).getContent();
    }

    @Override
    public void deleteArticle(long id) {
        repository.deleteById(id);
    }

    @Override
    public Article save(Article article) {
        return repository.saveAndFlush(article);
    }

}
