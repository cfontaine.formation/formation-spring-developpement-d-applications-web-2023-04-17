package fr.dawan.springweb.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.dawan.springweb.entities.Movie;

public interface MovieService {
    
    Page<Movie> getAllMovie(Pageable page);
    
    void deleteMovie(long id);
    
}
