package fr.dawan.springweb.services.impl;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springweb.entities.Movie;
import fr.dawan.springweb.repositories.MovieRepository;
import fr.dawan.springweb.services.MovieService;
@Service
@Transactional
public class MovieServiceImpl implements MovieService {

    @Autowired
    MovieRepository repository;
    
    @Override
    public Page<Movie> getAllMovie(Pageable page) {
        return repository.findAll(page);
    }

    @Override
    public void deleteMovie(long id) {
            repository.deleteById(id);
    }

  
}

