package fr.dawan.springweb.forms;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import fr.dawan.springweb.enums.Conditionement;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;

public class FormArticle {
    @NotEmpty(message = "ne doit être vide!!!!! ")
    @Size(max = 255)
    private String description;
    
    @NotNull
    @Min(value=0)
    private double prix;
    
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past
    private LocalDate dateProduction;
    
    @NotNull
    private Conditionement conditionement;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public Conditionement getConditionement() {
        return conditionement;
    }

    public void setConditionement(Conditionement conditionement) {
        this.conditionement = conditionement;
    }


}
