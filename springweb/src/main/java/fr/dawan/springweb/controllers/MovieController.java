package fr.dawan.springweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.springweb.entities.Movie;
import fr.dawan.springweb.services.MovieService;

@Controller
@RequestMapping("/store")
public class MovieController {

    @Autowired 
    private MovieService service;
    
    @GetMapping("/movies")
    public String getAllMovie(Pageable page,Model model) {
        List<Movie> movies=service.getAllMovie(page).getContent();
        model.addAttribute("movies",movies); 
        return "movies";
    }
    
    @GetMapping("/movies/delete/{id}")
    public String deleteArticle(@PathVariable long id) {
        service.deleteMovie(id);
        return "redirect:/store/movies";
    }

}
