package fr.dawan.springweb.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    @GetMapping("/erreur")
    public String handlerError(HttpServletRequest request,Model model)
    {
        Object status=request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if(status!=null) {
            int codeStatus=Integer.parseInt(status.toString());
            switch(codeStatus) {
            case 404:
                model.addAttribute("msgErr", "La page est introuvable");
                break;
            case 401:
                model.addAttribute("msgErr", "Vous n'étes pas authenfier");
                break;
            case 403:
                model.addAttribute("msgErr", "Vous n'avez pas l'authorisation");
                break;
            case 500:
                model.addAttribute("msgErr", "Erreur Interne");
                break;
                default:
                    model.addAttribute("msgErr", "Une erreur c'est produite");
                break;    
            }
            model.addAttribute("codeStatus", codeStatus);
        }
        return "error";
    }
}
