package fr.dawan.springweb.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springweb.entities.Personne;

@Controller
@RequestMapping("/exemple")
//-> @RequestMapping au niveau du controller: l'url préfixera toutes url des méthodes  
public class ExempleController {
    
    private static int cpt;
    
    @GetMapping
    public String exemple() {
        return "exemple";
    }

    // @RequestMapping => utilisée pour mapper les requêtes HTTP aux méthodes du
    // contrôleur
    // Model => utilisé pour transférer des données entre la vue et le contrôleur
    @RequestMapping(value = { "/testmodel", "/model" }, method = RequestMethod.GET)
    public String testModel(Model model) {
        String message = "Test Model";
        model.addAttribute("msg", message);
        return "exemple";
    }

    // ModelAndView => permet de transmettre le nom de la vue et les attributs avec
    // un seul return
    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        // ModelAndView mdv=new ModelAndView("exemple"); // ModelAndView("exemple") =>
        // On peut passer le nom de la vue au constructeur
        ModelAndView mdv = new ModelAndView(); // 2
        mdv.setViewName("exemple"); // ou utiliser la méthode setViewName
        mdv.addObject("msg", "Test ModelAndView");
        return mdv;
    }

    // params -> la méthode sera éxécutée
    // - si la requête a pour url /exemple/testparams
    // - et si la requéte a un paramètre id
    @GetMapping(value = "/testparams", params = "id")
    public String testParams(Model model) {
        model.addAttribute("msg", "La requête contient le paramètre id");
        return "exemple";
    }

    // params -> la méthode sera éxécutée :
    // - si la requête a pour url /exemple/testparams
    // - et si la requéte a un paramètre id et qu'il a pour valeur 42
    @GetMapping(value = "/testparamsv", params = "id=42")
    public String testParamsV(Model model) {
        model.addAttribute("msg", "La requête contient le paramètre id");
        return "exemple";
    }

    // @PathVariable
    @GetMapping("/testpath/{id}")
    public String testPathVariable(@PathVariable("id") String pathId, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + pathId);
        return "exemple";
    }

    @GetMapping("/testpathimpl/{id}")
    public String testPathVariableImpl(@PathVariable String id, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + id);
        return "exemple";
    }

    @GetMapping("/testpathmulti/{id}/action/{action}")
    public String testPathVariableMulti(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + id + "et action=" + action);
        return "exemple";
    }

    // @PathVariable => on peut lever les ambiguités en utilisant des expressions régulières
    @GetMapping("/testpathamb/{id:[0-9]+}") // id-> uniquement un nombre entier positif
    public String testPathVariableAmb1(@PathVariable String id, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + id);
        return "exemple";
    }

    @GetMapping("/testpathamb/{nom:[a-zA-Z]+}") // name -> uniquement des lettres minuscules et majuscule
    public String testPathVariableAmb2(@PathVariable String nom, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + nom);
        return "exemple";
    }

    // required -> pour rendre @PathVariable optionnel
    @GetMapping({ "/testpathoption/{id}", "/testpathoption" })
    public String testPathVariableOption(Model model, @PathVariable(required = false) String id) {
        model.addAttribute("msg", "l'url contient un paramètre id=" + id);
        return "exemple";
    }

    // @RequestParam
    @GetMapping("/testparam")
    public String testParam(@RequestParam("id") String idParam, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + idParam);
        return "exemple";
    }

    @GetMapping("/testparamimpl")
    public String testParamImpl(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + id);
        return "exemple";
    }

    @GetMapping("/testparamulti")
    public String testParaMulti(@RequestParam String id, @RequestParam String action, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + id + " action=" + action);
        return "exemple";
    }

    // @RequestParam => on peut lever les ambiguités en utilisant l'attribut params
    @GetMapping(value = "/testparamamb", params = "id") // on peut entrer dans la méthode uniquement, si la requête contient un paramètre id
    public String testParamAmb1(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + id);
        return "exemple";
    }

    @GetMapping(value = "/testparamamb", params = "nom") // on peut entrer dans la méthode uniquement, si la requête contient un paramètre nom
    public String testParamAmb2(@RequestParam String nom, Model model) {
        model.addAttribute("msg", "paramètre de requête nom=" + nom);
        return "exemple";
    }

    // @RequestParam => l'attribut defaultValue permet de définir une valeur par défaut pour le parmétre
    @GetMapping("/testparamdefault")
    public String testParamDefault(@RequestParam(defaultValue = "1") String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + id);
        return "exemple";
    }

    // @RequestParam => l'attribut required permet de rendre le paramètre optionnel
    @GetMapping("/testparamoption")
    public String testParamOption(@RequestParam(required = false) String id, Model model) {
        model.addAttribute("msg", "paramètre de requête id=" + id);
        return "exemple";
    }

    @PostMapping("/testformulaire")
    public String testFormulaire(@RequestParam String nom, Model model) {
        model.addAttribute("msg", nom);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testParamConv(@RequestParam int id, Model model) {
        model.addAttribute("msg", "le paramètre est convertit en entier=" + id);
        return "exemple";
    }

    @GetMapping("/testconvdate")
    public String testDateConv(Model model, @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam LocalDate debut) {
        model.addAttribute("msg", debut.toString());
        return "exemple";
    }

    // @RequestHeader => permet de récupérer un paramètre dans l'en-tête de la requête
    @GetMapping("/testheader")
    public String testHeader(@RequestHeader("user-agent") String userAgent, Model model) {
        model.addAttribute("msg", userAgent);
        return "exemple";
    }

    // HttpHeaders => pour récupérer tous les paramètres de la requête
    @GetMapping("/testallheader")
    public String testAllHeader(@RequestHeader HttpHeaders headers, Model model) {
        List<String> lstHeader = new ArrayList<>();
        Set<Entry<String, List<String>>> entry = headers.entrySet();
        for (Entry<String, List<String>> ent : entry) {
            String tmp = ent.getKey() + " : ";
            for (String s : ent.getValue()) {
                tmp += s + " ";
            }
            lstHeader.add(tmp);
        }
        model.addAttribute("lstHeader", lstHeader);
        return "exemple";
    }
    
    // Lier les beans
    @GetMapping("/testbindpath/{id}/{prenom}/{nom}")
    public String testBindPath(Personne per, Model model) {
        model.addAttribute("msg", per.toString());
        return "exemple";
    }

    @GetMapping("/testbindparam")
    public String testBindParam(Personne per, Model model) {
        model.addAttribute("msg", per.toString());
        return "exemple";
    }

    // Redirection
    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/hello"; // 302
    }

    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/hello";
    }
    
    // @ModelAttribute sur une méthode
   // @ModelAttribute("cpt")
//    public void testModelAttribute(Model model) {
//        model.addAttribute("cpt", cpt++);
//    }
    
    @ModelAttribute("cpt")
    public int testModelAttribute() {
        return cpt++;
    }
    
    // @ModelAttribute sur un paramètre de méthode
    @GetMapping("/testmodelattrparam")
    public String testModelAttrParam(@ModelAttribute("per1") Personne per1,Model model) {
        model.addAttribute("msg",per1.toString());
        return "exemple";
    }
    
    @ModelAttribute("per1")
    public Personne initPer1() {
        Personne p=new Personne("yves","roulot");
        p.setId(3L);
        return p;
    }
    
    // FlashAttribute
    @GetMapping("/testflash")
    public String testFlashAttribute(RedirectAttributes rAtt) {
        rAtt.addFlashAttribute("messageFlash","Vous venez d'ête redirigé");
        return "redirect:/exemple/cibleflash"; 
    }
    
    @GetMapping("/cibleflash")
    public String cibleFlash(@ModelAttribute("messageFlash") String msgFlash,Model model ) {
        model.addAttribute("msg", msgFlash );
        return "exemple";
    }
    
    @GetMapping("/exemplethymeleaf")
    public String testThymeleaf(@RequestParam(defaultValue ="-1") int val,Model model) {
        int i=42;
        model.addAttribute("i", i);
        
        Personne p=new Personne("John","Doe");
        p.setId(5L);
        model.addAttribute("john", p);
        
        double tab[]= {1.23,4.5,6.7};
        model.addAttribute("tab", tab);
        
        Map<String,Integer > m=new HashMap<>();
        m.put("toto",42);
        m.put("marcel", 23);
        model.addAttribute("m", m);
        
        model.addAttribute("val",val);
        model.addAttribute("valhtml","<b>un texte</b>");
        
        List<Personne> personnes=new ArrayList<>();
        personnes.add(new Personne("John","doe"));
        personnes.add(new Personne("Jane","doe"));
        personnes.add(new Personne("Yves","Roulot"));
        personnes.add(new Personne("Alan","Smithee"));
        model.addAttribute("personnes", personnes);
        
        return "exemplethymeleaf";
    }
    
    @GetMapping("/ioexception")
    public void genIOException() throws IOException {
        throw new IOException("Io Exception");
    }
    
    @GetMapping("/sqlexception")
    public void genSqlException() throws SQLException {
        throw new SQLException("SQL Exception");
    }
    
    @ExceptionHandler(IOException.class)
    public String handlerIOException(Exception e,Model model) {
        System.err.println(e.getMessage());
        for(StackTraceElement elm: e.getStackTrace()  ) {
            System.err.println(elm);
        }
        model.addAttribute("msgEx",e.getMessage());
        model.addAttribute("traceEx",e.getStackTrace());
        return "exception";
    }
    
    @PostMapping("/upload")
    String testUpload(@RequestParam("imgfile")MultipartFile mf) {
        System.out.println(mf.getOriginalFilename() +  " " +mf.getSize());
        File rep=new File("c:/Dawan/test");
        if(!rep.exists()) {
            rep.mkdir();
        }
        try (
            BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream("c:/Dawan/test/"
                    + mf.getOriginalFilename()))){
            
            bos.write(mf.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/exemple";
    }
    
}