package fr.dawan.springweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
    
	// @RequestMapping => utilisée pour mapper les requêtes HTTP aux méthodes du contrôleur
    @RequestMapping("/hello")
    public String helloworld() {
        return "home";	// on retourne le nom de la vue
    }

}
