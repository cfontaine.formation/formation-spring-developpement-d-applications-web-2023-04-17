package fr.dawan.springweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springweb.entities.Article;
import fr.dawan.springweb.services.ArticleService;

@RestController
@RequestMapping("/api/articles")
public class ArticleControllerWs {
    
    @Autowired
    ArticleService service;
    
    @ResponseStatus(code = HttpStatus.I_AM_A_TEAPOT)
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getAllArticles(){
        return service.getAllArticle();
    }
    
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public Article addArticle(@RequestBody Article article) {
        return service.save(article);
    }
    
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteArticle(@PathVariable long id) {
        try {
            service.deleteArticle(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("L'article id=" + id +" est supprimé",HttpStatus.OK);
    }

}
