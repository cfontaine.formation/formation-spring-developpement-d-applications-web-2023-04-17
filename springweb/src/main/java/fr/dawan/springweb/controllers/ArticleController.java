package fr.dawan.springweb.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.springweb.entities.Article;
import fr.dawan.springweb.forms.FormArticle;
import fr.dawan.springweb.services.ArticleService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/store")
public class ArticleController {

    @Autowired
    ArticleService service;
    
    @GetMapping("/articles")
    public String getAllArticle(Model model,Pageable page) {
       List<Article> articles=service.getAllArticle(page);
        model.addAttribute("articles", articles);
        return "articles";
    }
    
    @GetMapping("/articles/delete/{id}")
    public String deleteArticle(@PathVariable long id) {
        service.deleteArticle(id);
        return "redirect:/store/articles";
    }
    
    @GetMapping("/articles/add")
    public String addArticleGet(@ModelAttribute("formarticle") FormArticle articleForm) {
        return "addarticle";
    }
    
    @PostMapping("/articles/add")
    public String addArticlePost(@Valid @ModelAttribute("formarticle") FormArticle articleForm
                ,BindingResult results, Model model)
    {
        if(results.hasErrors())
        {
            model.addAttribute("formarticle", articleForm);
            model.addAttribute("errors",results);
            return "addarticle";
        }
        Article a=new Article(articleForm.getDescription(),articleForm.getPrix(),
                articleForm.getDateProduction(),articleForm.getConditionement());
        service.save(a);
        return "redirect:/store/articles";
    }
    
    @GetMapping("/articles/download")
    public void downloadArticle(HttpServletResponse response) {
        List<Article> lstArticle=service.getAllArticle();
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition","attachement;filename=articles.csv");
       try {
        ServletOutputStream o=response.getOutputStream();
        o.write("id;description;prix;dateProduction;conditionement".getBytes());
        for(Article a: lstArticle) {
            o.write((a.getId()+";"+a.getDescription()+";"+a.getPrix()+";"+
                    a.getDateProduction().toString()+";"+a.getConditionement().toString()).getBytes());
        }
        o.close();
       } catch (IOException e) {
        e.printStackTrace();
    }
    }
    
}