package fr.dawan.springweb.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
    
    @ExceptionHandler
    public String handlerException(Exception e, Model model) {
        System.err.println(e.getMessage());
        for(StackTraceElement elm: e.getStackTrace()  ) {
            System.err.println(elm);
        }
        model.addAttribute("msgEx","Global"+ e.getMessage());
        model.addAttribute("traceEx",e.getStackTrace());
        return "exception";
    }

}
