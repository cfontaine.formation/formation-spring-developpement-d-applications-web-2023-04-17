package fr.dawan.springweb.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.springweb.entities.Movie;


public interface MovieRepository extends JpaRepository<Movie, Long> {
    
    List<Movie> findByReleaseDateBefore(LocalDate dateMax);
    
    List<Movie> findByDurationBetween(int durationMin,int durationMax);
    
    List<Movie> findByTitleVFLike(String model);
    
    List<Movie> findByGenreName(String nomGenre);
    
    List<Movie> findByNationalityNameAndDurationLessThanOrderByDuration(String nomNation,int dureMin);
    
    @Query("SELECT m FROM Movie m JOIN m.directors d WHERE d.lastName=:name")
    List<Movie> getMovieByDirector(@Param("name") String name);
    
    @Query("SELECT m FROM Movie m JOIN m.directors d WHERE d.nationality.name=:pays and d.deathDate IS NOT NULL")
    List<Movie> getMovieByDirectorNation(@Param("pays") String pays);
}
