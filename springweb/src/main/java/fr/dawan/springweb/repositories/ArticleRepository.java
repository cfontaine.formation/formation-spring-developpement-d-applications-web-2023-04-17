package fr.dawan.springweb.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.springweb.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

   List<Article> findByPrixLessThan(double prixMax);
   
   List<Article> findByPrixBetween(double prixMin,double prixMax);
   
   List<Article> findByPrixGreaterThanAndDateProductionBefore(double prix,LocalDate dateProdMax);
   
   List<Article> findByDescriptionLike(String model);
   
   List<Article> findByPrixLessThanOrderByPrixAscDateProductionDesc(double prixMax);
   
   boolean existsByDateProductionAfter(LocalDate dateProd);
   
   int countByPrixLessThan(double prix);
   
   int deleteByPrixGreaterThan(double prixMin);
   //void deleteByPrixGreaterThan(double prixMin);
   
   List<Article> findByMarqueNomIgnoreCase(String nomMarque);
   
   // Fournisseur la relation avec Article =>@ManyToMany ->erreur
  // List<Article> findByFournisseurNom(String nomMarque);
   
   // JPQL
   @Query("SELECT a FROM Article a WHERE a.prix<:montant")
   List<Article> getArticleByPrixJPQL(@Param("montant") double prixMax);
   
   // SQL
   @Query(value="SELECT * FROM articles WHERE prix<?1",nativeQuery = true)
   List<Article> getArticleByPrixSQL(double prixMax);
   
   //List<Article> findByPrixLessThan(double prixMax,Pageable page);
   Page<Article> findByPrixLessThan(double prixMax,Pageable page);
   
   List<Article> findTop2ByPrixGreaterThanOrderByPrixDesc(double prixMax);
}
